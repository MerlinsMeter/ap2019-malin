![ScreenShot](Mini_ex02.PNG)

https://cdn.staticaly.com/gl/MerlinsMeter/ap2019-malin/raw/master/Mini_ex02Imagine/empty-example/index.html

For this weeks project we were supposed to design two emojis, play around with shapes, colour, et cetera.
Clearly, I haven't really done that. In stead I decided to make something a little different.

To make this a little more clear I have to explain my thought and work process.

My first idea for this weeks mini exercise was, to let the user of my programm play around with the shape
of the generic emoji-shape, which in most cases is a circle.
So, I made two ellipses in the colour purple. I wanted to  pick the colour that was the furthest away
from yellow on the colourwheel, hence purple.

To change the shape I created two movable shapes that change depending on, where on the canvas the mouse is.

So, my first idea was to manipulate and challenge the normal emoji shape and colour.

But once I had that down, I realized that the two circles looked very much like eyes. But instead of
changing that, I decided to roll with it.
In the end I think, that it worked out very well.

“Westerners look at the eyes and the mouth in equal measure, whereas Easterners favor the eyes and neglect the mouth."
This quote comes from the following homepage: https://www.japantimes.co.jp/news/2009/10/11/national/science-health/in-cross-cultural-situations-remember-those-emoticons/#.XGrEl_rQhPY

So, I want to encourage you to play around with the shapes and expressions you can create both while focusing on the eyes 
and the face shape.


__The programm__

For this week I focussed on, how I can use variables together with shapes and the functions mouseX and mouseY, which was more complicated
than I had thought. But it worked out very well in the end. But I had to utilize a notepad and a pen to better visualize what I wanted to 
create.
