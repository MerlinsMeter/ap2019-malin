![screenShot](Mini_ex04ICU.PNG)

https://cdn.staticaly.com/gl/MerlinsMeter/ap2019-malin/raw/master/Mini_ex04ICU/empty-example/index.html

For this mini exercise i took inspiration in an eye tracking device that was popular in some youtube videos. The device
tracks the eye movement and translates it to, where on the screen someone is looking.

I changed it up a bit and made it  mouse tracker instead.
To acieve this I made the colour of the ellipse, that is attached to the mouses movement,
change colour with every press, using the function mouseIsPressed. 

Because of the low opacity of the coulour, it is also possible to see, how long the mouse stayed on cerstain parts of the screen.
Of course this in no reliable science behind that function, but by looking at it, there is the general idea.

While I made this program I thought bout surveilance, both from outside sources and from the user itself.
While such a program might be useful for market research, just like the eye tracking device might be,
it could also be used for the racking of habits and self observation. On youtube it was a trend to use the eye tracker to look
at suggestive pictures and try not to look where it would be rude to look.

Such trackers might help to see behavioral patterns that we weren't aware of before. Of course, there is a bg difference. While something
like looking at certain things might be out of reflex, somthing like moving a mouse is easier to control and more intentional.
But while that is the case it migh still give people other than ourselves insight to, what we are doing on our computers, which 
for most people is a frightening thought.