![screenShot](mini_ex03.PNG)

https://cdn.staticaly.com/gl/MerlinsMeter/ap2019-malin/raw/master/Mini_ex03Patience/p5/empty-example/index.html

__Mini_ex - Patience__

For this mini exercise I wanted to do a cute little animation. The silhuette of a fischerman with a fishing rod immediatly popped into 
my mind when we talked about throbbers, so that is what I went with.

I myself never went fishing, as dead fish scare me, and I don't like eating fish in general, but for me fishing is something, 
that can end in either success or failure. One could sit in one spot for an entire afternoon without any success, while 
that could change completly the next day.
And generally, there is no way to see, when something will bite, or what it is going to be.
So that was the general idea.

From the technical standpoint, i had to use push() and pop() quite a bit to seperate different pieces of code. Push and pop
made a huge difference when it came to, how the ellipses moved while the image still was intact.

I also used translate() to move 0,0 to windowWidth ans windowHeight to make it easier to place the circles, as it was easier to 
calculate that way.

Tho make the animation I had to make a variable for every circle that I wanted to move, so that they could move independently from each other. Before I did that, the circles
just jumped back to their position once they had grown as far as I wanted them, which didn't look right. Once every circle had its own variable, it was easier for me,
to play around with the numbers to make the movements of the rings as smooth as possible.

I wanted the image to be in the center in every browser and on every screen, so I didn't use any actual coordinates (just windowWidth/windowHeight woth some calculations)
to achieve that. (I hope it works, I have only tried
it on my phone and on my laptop :) )


__Throbbers in general__

During my time as an internet user, i have encoutered numerous throbbers: those, that are barely noticable, and those that seem to last forever and just make you restart
the entire page. The most known ones that i have seen are probaply the little spinny things that appear next to the coursor, in browser windows, and on youtube videos.
But there are also more unique ones, especially im mobile games, that i have come across. These throbbers are more unique and fit to the game to keep
the player engrossed in the game.

As an overall conclusion I can say that, even though throbbers sometimes have frustrated me, and surely will continue to do so in the future, I am thankful 
that they exist. Nothing is more frustrating, not even a throbber, than a web page or document (that wasn't saved) that just freezes without any indication,
that something is happening behind the scenes.
Throbbers may je used to cover, what happens in the background or to hold up an certain image (for example "false latency"), but it is not always realistic,
that a user wants to know, what happens behind the scenes.

And in a world, where everything just keeps going faster and faster, isn't it okay, to not set peoples expectations to high when it comes to loading
speed? 