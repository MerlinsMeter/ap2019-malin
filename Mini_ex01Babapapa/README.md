![Screenshot](Mini_ex01.PNG)

__This weeks project:__

For this project I used an if-else-statement to keep the pupil inside of the eyeball. The same sentence
makes the eye shut once the mouse is outside of the eyeball-ellipse. 
To give the half-face a bit more expression I made an ellipse to represent the eyebrow.
I changed the 0,0 point to be at the center of the eye using translate so that the brow can rotate around it
to give the eye a frown depeding on, where the pupil is in the eye (far left and far right makes equals a frown).

https://cdn.staticaly.com/gl/MerlinsMeter/ap2019-malin/raw/master/Mini_ex01Babapapa/p5/empty-example/index.html


__My first coding experience:__

My first coding experience was very positive At first it was hard to come up with an idea for, what I want to code, as there 
were no direct guidelines. But I think that makes it more fun, as it enhances the creative process.

I found coding surprisingly easy. That is a bit provocative to say, so I think "It was easier than I had expected" may sound better.
Going into this I was afraid that coding would be to logical for me, as I really had difficulties with logical subjects like math
and chemics. But I was relieved to see that coding was in a way logical, that I could still understand and comprehend it without
my typical frustrations, so that was very nice.

Handing in the project was...more frustrating that the mini exercise itself. But I try to stay positive because I know
that I will get the hang of it eventually.

Am am as of now still unsure what I am expected to write about my mini exercises every week, so I just wrote something I
felt was right this week.


__Coding versus reading and writing:__

I feel like coding is both similar and different depending on what kind of texts I read. If it is a book i read in my spare time I
think it is very different, as i tend to just go with the flow and read without thinking.

But compared to academic texts and study material I can see a few similarities. Sometimes it is necerssary to go over a specific section over
and over again to understand it, and it is required to think during the process of reading or coding.


__What coding is to me:__

For me, coding isn't anything...yet. I feel like I am way to new to coding to say anything about it.
But what coding might be for me in the future could be a way to approach a subject in an organized way and maybe a way for me to
tinker with something. I had a lot of fun with it this week, so I think, coding could also become a source of both satisfaction and 
frustration, depending on how my projects turn out.




